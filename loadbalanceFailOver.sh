#!/bin/bash
 
# Configure as variáveis iface1,iface2,gw1,gw2 de acordo com sua rede ###
iface1="eth0" # interface do link padrão
iface2="eth2" # interface do link alternativo
gw1="192.168.0.1" # gateway do link padrão
gw2="192.168.1.1" # gateway do link alternativo
#
netmask="0.0.0.0" # deixe assim.
error=0
#
sleep_test=3
# logs para informações de funcionamento
log="/tmp/lb-loadbalance.log"
> $log
# Habilita o roteamento:
echo "1" > /proc/sys/net/ipv4/ip_forward

source "./lib/start_services"
source "./lib/iptables_roles"
source "./lib/check_internet"

function link_alternativo() {
    
    route add default gw $gw2 netmask $netmask dev $iface2

    while :; do
        
        # Valida comunicação com a internet para link 2
        status=$(check_internet $iface2)
        status_link_principal=$(check_internet $iface1)
        if [ $status -ne 0 ] || [ $status_link_principal -eq 0 ]; then
            route del default gw $gw2 netmask $netmask dev $iface2
            link_principal
            break
        else
            echo "Link $iface2, Gateway $gw2 OK!" >> $log
        fi
        tail $log
        sleep $sleep_test
    done
}

function link_principal() {
    
    route add default gw $gw1 netmask $netmask dev $iface1
    
    while :; do

        # Valida comunicação com a internet para link 1
        status=$(check_internet $iface1)
        echo "status: $status"
        if [ $status -ne 0 ]; then
            route del default gw $gw1 netmask $netmask dev $iface1
            link_alternativo
            break
        else
            echo "Link $iface1, Gateway $gw1 OK!" >> $log
        fi
        tail $log
        sleep $sleep_test
    done
}

link_principal

# kill -9 $(ps aux | grep loadbalanceFailOver | awk '{print $2}') && cd /home/pi/loadbalanceFailOver && bash loadbalanceFailOver.sh
# # Marcelo Viana
# # infsite.org
# # AMDG
